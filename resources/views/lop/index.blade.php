<a href="{{ route('lop.create') }}">
	Thêm
</a>
<table border="1" width="100%">
	<caption>
		<form>
			Tim kiem theo ten
			<input type="search" name="tim_kiem" value="{{$tim_kiem}}">
		</form>
	</caption>
	<tr>
		<th>Tên</th>
		<th>Tuổi</th>
		<th>Xem</th>
		<th>Sửa</th>
		<th>Xóa</th>
	</tr>
	@foreach ($array_lop as $lop)
		<tr>
			<td>
				{{$lop->ten}}
			</td>
			<td>
				{{$lop->tuoi}}
			</td>
			<td>
				<a href="{{ route('lop.show',['ma' => $lop->ma]) }}">
					Xem
				</a>
			</td>
			<td>
				<a href="{{ route('lop.edit',['ma' => $lop->ma]) }}">
					Sua
				</a>
			</td>
			<td>
				<form action="{{ route('lop.destroy',['ma' => $lop->ma]) }}" method="post">
					{{csrf_field()}}
					<input type="hidden" name="_method" value="DELETE">
					<button>Xoa</button>
				</form>
			</td>
		</tr>
	@endforeach
</table>
{{$array_lop->appends(['tim_kiem' => $tim_kiem])->links()}}