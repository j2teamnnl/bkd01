@extends('layout.master')
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}">
@endpush
@section('content')
<form action="{{ route('giua_nam.process_chuyen_lop') }}" method="post">
	{{ csrf_field() }}
	Chọn những sinh viên để chuyển lớp
	<select id="select_sinh_vien" multiple name="array_sinh_vien[]">
		@foreach ($array_sinh_vien as $sinh_vien)
			<option value="{{ $sinh_vien->ma }}">
				{{ $sinh_vien->ten }} ({{ $sinh_vien->ma }})
			</option>
		@endforeach
	</select>
	<br>
	Chọn lớp muốn chuyển tới
	<select id="select_lop" name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma }}">
				{{ $lop->ten }}
			</option>
		@endforeach
	</select>
	<button>Chuyển lớp</button>
</form>
@endsection

@push('js')
<script type="text/javascript" src='{{ asset('js/select2.min.js') }}'></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#select_sinh_vien").select2();
});
</script>
@endpush