@extends('layout.master')

@section('content')
<h1>
	@lang('welcome.xin_chao',['ten' => Session::get('ten')])
	{{ trans_choice('welcome.gioi_tinh',0) }}
</h1>
<a href="{{ route('logout') }}">
	@lang('welcome.dang_xuat')
</a>
@endsection