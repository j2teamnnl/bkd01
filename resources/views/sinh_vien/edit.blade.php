<form action="{{ route('sinh_vien.update',['ma' => $sinh_vien->ma]) }}" method="post">
	<input type="hidden" name="_method" value="PUT">
	{{csrf_field()}}
	Ten
	<input type="text" name="ten" value="{{$sinh_vien->ten}}">
	<br>
	Ngay sinh
	<input type="date" name="ngay_sinh" value="{{$sinh_vien->ngay_sinh}}">
	<br>
	Chọn lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{$lop->ma}}"
			@if ($lop->ma == $sinh_vien->ma_lop)
				selected 
			@endif
			>
				{{$lop->ten}}
			</option>
		@endforeach
	</select>
	<button>Sua</button>
</form>