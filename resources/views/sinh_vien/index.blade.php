<a href="{{ route('sinh_vien.create') }}">
	Thêm
</a>
<br>
<a href="{{ route('sinh_vien.view_insert_with_excel') }}">
	Thêm bằng excel
</a>
<br>
<a href="{{ route('sinh_vien.choose_by_lop') }}">
	Xem theo lop
</a>
<table border="1" width="100%">
	<caption>
		<form>
			Tim kiem theo ten
			<input type="search" name="tim_kiem" value="{{$tim_kiem}}">
		</form>
	</caption>
	<tr>
		<th>Tên</th>
		<th>Tuổi</th>
		<th>Gioi tinh</th>
		<th>Ảnh</th>
		<th>Ten Lop</th>
		<th>Xem</th>
		<th>Sửa</th>
		<th>Xóa</th>
	</tr>
	@foreach ($array_sinh_vien as $sinh_vien)
		<tr>
			<td>
				{{$sinh_vien->ten}}
			</td>
			<td>
				{{$sinh_vien->tuoi}}
			</td>
			<td>
				{{$sinh_vien->ten_gioi_tinh}}
			</td>
			<td>
				<img src="{{asset("uploads/anh_sinh_vien/$sinh_vien->anh")}}" height="200">
				<a href="{{ route('sinh_vien.view_change_anh',['ma' => $sinh_vien->ma]) }}">
					Đổi ảnh
				</a>
			</td>
			<td>
				{{$sinh_vien->lop->ten}}
			</td>
			<td>
				<a href="{{ route('sinh_vien.show',['ma' => $sinh_vien->ma]) }}">
					Xem
				</a>
			</td>
			<td>
				<a href="{{ route('sinh_vien.edit',['ma' => $sinh_vien->ma]) }}">
					Sua
				</a>
			</td>
			<td>
				<form action="{{ route('sinh_vien.destroy',['ma' => $sinh_vien->ma]) }}" method="post">
					{{csrf_field()}}
					<input type="hidden" name="_method" value="DELETE">
					<button>Xoa</button>
				</form>
			</td>
		</tr>
	@endforeach
</table>
{{$array_sinh_vien->appends(['tim_kiem' => $tim_kiem])->links()}}