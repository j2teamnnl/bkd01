<form action="{{ route('sinh_vien.view_by_lop') }}">
	Chọn lớp để xem
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{$lop->ma}}" 
			@if ($lop->ma == $ma_lop)
				selected 
			@endif>
				{{$lop->ten}}
			</option>
		@endforeach
	</select>
	<button>OK</button>
</form>
<a href="{{ route('sinh_vien.create',['ma_lop' => $ma_lop]) }}">
	Thêm
</a>
<table border="1" width="100%">
	<tr>
		<th>Tên</th>
		<th>Ngay sinh</th>
		<th>Xem</th>
		<th>Sửa</th>
		<th>Xóa</th>
	</tr>
	@foreach ($array_sinh_vien as $sinh_vien)
		<tr>
			<td>
				{{$sinh_vien->ten}}
			</td>
			<td>
				{{$sinh_vien->ngay_sinh}}
			</td>
			<td>
				<a href="{{ route('sinh_vien.show',['ma' => $sinh_vien->ma]) }}">
					Xem
				</a>
			</td>
			<td>
				<a href="{{ route('sinh_vien.edit',['ma' => $sinh_vien->ma]) }}">
					Sua
				</a>
			</td>
			<td>
				<form action="{{ route('sinh_vien.destroy',['ma' => $sinh_vien->ma]) }}" method="post">
					{{csrf_field()}}
					<input type="hidden" name="_method" value="DELETE">
					<button>Xoa</button>
				</form>
			</td>
		</tr>
	@endforeach
</table>