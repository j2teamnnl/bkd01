<form action="{{ route('sinh_vien.view_by_lop') }}">
	Chọn lớp để xem
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{$lop->ma}}">
				{{$lop->ten}}
			</option>
		@endforeach
	</select>
	<button>OK</button>
</form>