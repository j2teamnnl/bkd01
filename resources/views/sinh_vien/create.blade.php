@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('sinh_vien.store') }}" method="post">
	{{csrf_field()}}
	Ten
	<input type="text" name="ten" value="{{old('ten')}}">
	<br>
	Ngay Sinh
	<input type="date" name="ngay_sinh" value="{{old('ngay_sinh')}}">
	<br>
	Giới tính
	<input type="radio" name="gioi_tinh" value="1">Nam
	<input type="radio" name="gioi_tinh" value="0">Nữ
	<br>
	<input type="" name="">
	Chọn lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{$lop->ma}}"
			@if ($lop->ma == $ma_lop)
				selected 
			@endif
			>
				{{$lop->ten}}
			</option>
		@endforeach
	</select>
	<br>
	<button>Them</button>
</form>