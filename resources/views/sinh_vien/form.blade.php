@extends('layout.master')
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}">
@endpush

@section('content')
<form>
	Chọn lớp
	<select id="select_lop">
		<option disabled selected>
			Hãy chọn lớp
		</option>
		@foreach ($array_lop as $lop)
			<option value="{{$lop->ma}}">
				{{$lop->ten}}
			</option>
		@endforeach
	</select>
	<div>
		Chọn sinh viên
		<select id="select_sinh_vien" style="width: 100px;">
			
		</select>
	</div>	
</form>

@endsection

@push('js')
<script type="text/javascript" src='{{ asset('js/select2.min.js') }}'></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#select_lop").select2();
	$("#select_sinh_vien").select2({
		ajax: {
			url: '{{ route('ajax') }}',
			dataType: 'json',
			type: 'GET',
			data: function (params) {
		      	return {
		        	ten: params.term,
		        	ma_lop: $("#select_lop").val()
		      	};
		    },
		    processResults: function (data) {
			    return {
			        results: $.map(data, function(obj) {
			            return { 
			            	id: obj.ma, 
			            	text: obj.ten 
			            };
			        })
			    };
			},
			placeholder: 'Search for a repository'
		}
	});
});
</script>
@endpush