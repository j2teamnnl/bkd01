<ul>
	<li>
		Đầu năm học
		<ul>
			<li>
				<a href="{{ route('dau_nam.lop.index') }}">
					Xem Lop
				</a>
			</li>
			<li>
				<a href="{{ route('dau_nam.sinh_vien.index') }}">
					Xem Sinh Vien
				</a>
			</li>
		</ul>
	</li>
	<li>
		Giữa năm học
		<ul>
			<li>
				<a href="{{ route('giua_nam.view_chuyen_lop') }}">
					Chuyển lớp
				</a>
			</li>
			<li>
				<a href="{{ route('giua_nam.view_cap_nhat_sinh_vien_by_excel') }}">
					Cập nhật thông tin sinh viên bằng file Excel
				</a>
			</li>
		</ul>
	</li>
	
</ul>