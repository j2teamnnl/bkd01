<!DOCTYPE html>
<html>
<head>
	<title></title>
	@stack('css')
</head>
<body>
<div>
	@include('layout.banner')
</div>
<div>
	@include('layout.menu')
</div>
<div style="color:red">
	@yield('content')
</div>
<div>
	day la footer
</div>
<script type="text/javascript" src='{{ asset('js/jquery.js') }}'></script>
@stack('js')
</body>
</html>