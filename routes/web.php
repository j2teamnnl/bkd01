<?php

Route::get('doi_ngon_ngu/{ngon_ngu}',function($ngon_ngu){
	Session::put('ngon_ngu',$ngon_ngu);
});

Route::get('test','Controller@test');


Route::get('','LoginController@view_login')->name('view_login');
Route::post('process_login','LoginController@process_login')->name('process_login');

Route::group(['middleware' => 'CheckLogin'],function(){
	Route::get('welcome','Controller@welcome')->name('welcome');

	Route::get('form','SinhVienController@form');
	Route::get('ajax','SinhVienController@ajax')->name('ajax');

	Route::group(['as' => 'dau_nam.', 'prefix' => 'dau_nam'],function(){
		Route::group(['as' => 'sinh_vien.', 'prefix' => 'sinh_vien'],function(){
			Route::get('choose_by_lop','SinhVienController@choose_by_lop')->name('choose_by_lop');
			Route::get('view_by_lop','SinhVienController@view_by_lop')->name('view_by_lop');

			// đổi ảnh
			Route::get('view_change_anh/{ma}','SinhVienController@view_change_anh')->name('view_change_anh');
			Route::post('process_change_anh/{ma}','SinhVienController@process_change_anh')->name('process_change_anh');

			// làm việc với file excel
			Route::get('view_insert_with_excel','SinhVienController@view_insert_with_excel')->name('view_insert_with_excel');
			Route::post('process_insert_with_excel','SinhVienController@process_insert_with_excel')->name('process_insert_with_excel');

			Route::get('export_excel_from_view_all','SinhVienController@export_excel_from_view_all')->name('export_excel_from_view_all');
		});
		Route::resource('sinh_vien','SinhVienController');




		Route::resource('lop','LopController');
	});
	Route::group(['as' => 'giua_nam.', 'prefix' => 'giua_nam'],function(){
		// form chuyển lớp hàng loạt sinh viên
		Route::get('view_chuyen_lop','GiuaNamController@view_chuyen_lop')->name('view_chuyen_lop');
		Route::post('process_chuyen_lop','GiuaNamController@process_chuyen_lop')->name('process_chuyen_lop');

		// form nhập file excel thông tin sinh viên
		Route::get('view_cap_nhat_sinh_vien_by_excel','GiuaNamController@view_cap_nhat_sinh_vien_by_excel')->name('view_cap_nhat_sinh_vien_by_excel');
		Route::post('process_cap_nhat_sinh_vien_by_excel','GiuaNamController@process_cap_nhat_sinh_vien_by_excel')->name('process_cap_nhat_sinh_vien_by_excel');
	});
	Route::get('logout','LoginController@logout')->name('logout');
});

Route::get('send_mail','Controller@send_mail');

