<?php

use Illuminate\Database\Seeder;

use App\Models\SinhVien;

class SinhVienSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(SinhVien::class, 10)->create();
    }
}
