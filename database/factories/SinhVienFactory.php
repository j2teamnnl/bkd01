<?php

use App\Models\SinhVien;
use App\Models\Lop;

$factory->define(SinhVien::class, function () {
	$faker = Faker\Factory::create('vi_VN');
    return [
		'ten'       => $faker->name,
		'ngay_sinh' => $faker->dateTimeBetween('-30 years', '-20 years'),
		'gioi_tinh' => $faker->boolean,
		'anh'       => $faker->imageUrl(200, 200),
		'ma_lop'    => function () {
            return Lop::inRandomOrder()->value('ma');
        },
    ];
});
