<?php

namespace App\Http\Controllers;

use App\Models\Lop;
use Illuminate\Http\Request;

class LopController extends Controller
{
    protected $table = 'lop';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tim_kiem = $request->get('tim_kiem');
        $array_lop = Lop::where('ten','like',"%$tim_kiem%")->paginate(1);
        return view("$this->table.index",[
            'array_lop' => $array_lop,
            'tim_kiem' => $tim_kiem,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("$this->table.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lop = new Lop;
        $lop->ten = $request->get('ten');
        $lop->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lop  $Lop
     * @return \Illuminate\Http\Response
     */
    public function show($ma)
    {
        // $lop = Lop::where('ma',$ma)->first();
        $lop = Lop::find($ma);
        return $lop->ten;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lop  $Lop
     * @return \Illuminate\Http\Response
     */
    public function edit($ma)
    {
        $lop = Lop::find($ma);

        return view("$this->table.edit",[
            'lop' => $lop
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lop  $Lop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ma)
    {
        $lop = Lop::find($ma);
        $lop->ten = $request->get('ten');
        $lop->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lop  $Lop
     * @return \Illuminate\Http\Response
     */
    public function destroy($ma)
    {
        Lop::find($ma)->delete();
    }
}
