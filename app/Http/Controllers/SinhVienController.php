<?php

namespace App\Http\Controllers;

use App\Models\SinhVien;
use App\Models\Lop;
use Illuminate\Http\Request;
use App\Imports\SinhVienImport;
use Excel;
use Storage;
use App\Exports\SinhVienExport;
use App\Http\Requests\SinhVienRequest;

class SinhVienController extends Controller
{
    protected $table = 'sinh_vien';

    public function index(Request $request)
    {
        $tim_kiem = $request->get('tim_kiem');
        $array_sinh_vien = SinhVien::where('sinh_vien.ten','like',"%$tim_kiem%")
        ->paginate(2);
        return view("$this->table.index",[
            'array_sinh_vien' => $array_sinh_vien,
            'tim_kiem'        => $tim_kiem,
        ]);
    }

    public function create(Request $request)
    {
        $array_lop = Lop::get();

        $ma_lop = $request->get('ma_lop');
        return view("$this->table.create",compact('array_lop','ma_lop'));
    }

    public function store(SinhVienRequest $request)
    {
        SinhVien::create($request->all());
    }

    public function show($ma)
    {
        // $sinh_vien = SinhVien::where('ma',$ma)->first();
        $sinh_vien = SinhVien::find($ma);
        return $sinh_vien->ten;
    }

    public function edit($ma)
    {
        $sinh_vien = SinhVien::find($ma);
        $array_lop = Lop::get();

        return view("$this->table.edit",[
            'sinh_vien' => $sinh_vien,
            'array_lop' => $array_lop,
        ]);
    }

    public function update(Request $request, $ma)
    {
        $sinh_vien = SinhVien::find($ma);
        $sinh_vien->ten = $request->get('ten');
        $sinh_vien->ngay_sinh = $request->get('ngay_sinh');
        $sinh_vien->ma_lop = $request->get('ma_lop');
        $sinh_vien->save();

    }

    public function destroy($ma)
    {
        SinhVien::find($ma)->delete();
    }

    public function choose_by_lop()
    {
        $array_lop = Lop::get();
        return view("$this->table.choose_by_lop",compact('array_lop'));
    }

    public function view_by_lop(Request $rq)
    {
        $ma_lop = $rq->get('ma_lop');

        $array_sinh_vien = SinhVien::where('ma_lop',$ma_lop)->get();

        $array_lop = Lop::get();

        return view("$this->table.view_by_lop",compact('array_sinh_vien','array_lop','ma_lop'));
    }
    public function view_insert_with_excel()
    {
        return view("$this->table.view_insert_with_excel");
    }
    public function process_insert_with_excel(Request $rq)
    {
        Excel::import(new SinhVienImport, $rq->file('excel_sinh_vien')->path());
    }
    public function export_excel_from_view_all()
    {
         return Excel::download(new SinhVienExport, 'sinh_vien_list_all.xlsx');
    }
    public function view_change_anh($ma)
    {
        return view("$this->table.view_change_anh",['ma' => $ma]);
    }
    public function process_change_anh(Request $rq, $ma)
    {
        $file_anh = $rq->file('anh_sinh_vien');

        $ten_file_anh  = time().".".$file_anh->extension();

        Storage::disk('public')->putFileAs("anh_sinh_vien", $file_anh, $ten_file_anh);
        
        SinhVien::find($ma)->update(['anh' => $ten_file_anh]);
    }
    public function form()
    {
        $array_lop = Lop::get();

        return view('sinh_vien.form',compact('array_lop'));
    }
    public function ajax(Request $rq)
    {
        $ma_lop = $rq->get('ma_lop');
        $ten    = $rq->get('ten');
        $array_sinh_vien = SinhVien::where('ma_lop',$ma_lop)
        ->where('ten','like',"%$ten%")
        ->get();

        return $array_sinh_vien;
    }
}
