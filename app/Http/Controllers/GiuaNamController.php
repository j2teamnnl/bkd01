<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SinhVien;
use App\Models\Lop;
use Excel;
use App\Imports\SinhVienUpdateOrCreateImport;

class GiuaNamController extends Controller
{
    public function view_chuyen_lop()
    {
    	$array_sinh_vien = SinhVien::get();

    	$array_lop = Lop::get();

    	return view('giua_nam.view_chuyen_lop',[
    		'array_sinh_vien' => $array_sinh_vien,
    		'array_lop' => $array_lop,
    	]);
    }
    public function process_chuyen_lop(Request $rq)
    {
    	$array_sinh_vien = $rq->array_sinh_vien;
    	$ma_lop = $rq->ma_lop;

    	// SinhVien::whereIn('ma',$array_sinh_vien);
    	SinhVien::whereIn('ma',$array_sinh_vien)->update([
    		'ma_lop' => $ma_lop
    	]);
    }
    public function view_cap_nhat_sinh_vien_by_excel()
    {
    	return view('giua_nam.view_cap_nhat_sinh_vien_by_excel');
    }
    public function process_cap_nhat_sinh_vien_by_excel(Request $rq)
    {
    	$file = $rq->file('excel_sinh_vien');

    	Excel::import(new SinhVienUpdateOrCreateImport, $file);
    }
}
