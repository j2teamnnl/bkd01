<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SinhVienRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ten' => 'required|min:2',
            'ngay_sinh' => 'required|date',
            'ma_lop' => 'exists:lop,ma',
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute không được phép để trống',
            'min' => 'Độ dài :attribute không được phép để nhỏ hơn :min ký tự',
            'exists' => ':attribute phải tồn tại',
        ];
    }
    public function attributes()
    {
        return [
            'ten' => 'Tên',
            'ngay_sinh' => 'Ngày sinh',
            'ma_lop' => 'Lớp',
        ];
    }
}
