<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lop extends Model
{
    protected $table = 'lop';
    protected $fillable = ['ten'];
    protected $primaryKey = 'ma';

    public $timestamps = false;
    public function sinh_viens()
    {
        return $this->hasMany('App\Models\SinhVien','ma_lop','ma');
    }
}
