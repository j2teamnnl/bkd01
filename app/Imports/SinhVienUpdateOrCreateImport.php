<?php

namespace App\Imports;

use App\Models\SinhVien;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;

class SinhVienUpdateOrCreateImport implements WithHeadingRow,ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            SinhVien::updateOrCreate([
                'ma' => $row['ma_sinh_vien']
            ],[
                'ten' => $row['ten_sv'],
                'ngay_sinh' => date_format(date_create($row['ngay_sinh']),'Y-m-d'),
            ]);
        }
    }
}
