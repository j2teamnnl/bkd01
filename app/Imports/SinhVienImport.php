<?php

namespace App\Imports;

use App\Models\SinhVien;
use App\Models\Lop;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SinhVienImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = [
            'ten'       => $row['ten'],
            'ngay_sinh' => date_format(date_create($row['ngay_sinh']),'Y-m-d'),
            'gioi_tinh' => ($row['gioi_tinh']=='Nam') ? 1 : 0,
            // select ma from lop where ten = 'BKD' limit 1
            // if not exist insert into lop(ten) values ('BKD')
            // select ma from lop where ten = 'BKD' limit 1
            'ma_lop'    => Lop::firstOrCreate(['ten' => $row['lop']])->ma
        ];
        return new SinhVien($data);
    }
}
