<?php

namespace App\Exports;

use App\Models\SinhVien;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SinhVienExport implements FromView
{
    public function view(): View
    {
        return view('sinh_vien.list_all', [
            'array_sinh_vien' => SinhVien::all()
        ]);
    }
}
